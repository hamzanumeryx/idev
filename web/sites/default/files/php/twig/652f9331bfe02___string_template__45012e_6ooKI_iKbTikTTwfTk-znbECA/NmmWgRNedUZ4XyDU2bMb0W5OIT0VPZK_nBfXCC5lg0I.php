<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* __string_template__45012e97d66c0e844854053a27eccf54 */
class __TwigTemplate_8f018aec66f3d514baa72122c442e519 extends \Twig\Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->parent = false;

        $this->blocks = [
        ];
        $this->sandbox = $this->env->getExtension('\Twig\Extension\SandboxExtension');
        $this->checkSecurity();
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 1
        echo "    <div class=\"container pi-60\">
        <div class=\"flex-chiffres\">
        <div class=\"container_chiffres\">
        <div class=\"icone_formation\"> <i> <img src=\"/idev/web/sites/default/files/media-icons/icone-tableau.png\" alt=\"Description of the icon\"  class=\"icone-chiffre\"></i></div>
        <div>
        <div class=\"chiffres\" id=\"value\">";
        // line 6
        echo $this->extensions['Drupal\Core\Template\TwigExtension']->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed(($context["field_chiffres_formations"] ?? null), 6, $this->source), "html", null, true);
        echo "</div>
        <div class=\"titre_chiffres\">Formations effectuées</div>
        </div>
        </div>
        <div class=\"container_chiffres\" >
        <div class=\"icone_participant\"> <i> <img src=\"/idev/web/sites/default/files/media-icons/icone-people.png\" alt=\"Description of the icon\"  class=\"icone-chiffre\"></i></div>
        <div>
        <div class=\"chiffres\" id=\"value1\">";
        // line 13
        echo $this->extensions['Drupal\Core\Template\TwigExtension']->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed(($context["field_chif"] ?? null), 13, $this->source), "html", null, true);
        echo "</div>
        <div class=\"titre_chiffres\">Participants </div>
         </div>
        </div>
        <div class=\"container_chiffres\" >
        <div class=\"icone_pays\"><i class=\"fa-regular fa-flag\"></i></div>
        <div>    
        <div class=\"chiffres\" id=\"value2\">";
        // line 20
        echo $this->extensions['Drupal\Core\Template\TwigExtension']->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed(($context["field_ch"] ?? null), 20, $this->source), "html", null, true);
        echo "</div>
        <div class=\"titre_chiffres\">Pays</div>
        </div>
        </div>
        <div class=\"container_chiffres\">
            <div class=\"icone_mondats\"> <i> <img src=\"/idev/web/sites/default/files/media-icons/icone-fichier.png\" alt=\"Description of the icon\"  class=\"icone-chiffre\"></i></div>
               <div> 
            <div class=\"chiffres\" id=\"value3\">";
        // line 27
        echo $this->extensions['Drupal\Core\Template\TwigExtension']->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed(($context["field_chiffres_mondats_realises"] ?? null), 27, $this->source), "html", null, true);
        echo "</div>
            <div class=\"titre_chiffres\">Mandats réalisés</div>
             </div>
            </div>
        </div>
        </div>";
    }

    public function getTemplateName()
    {
        return "__string_template__45012e97d66c0e844854053a27eccf54";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  76 => 27,  66 => 20,  56 => 13,  46 => 6,  39 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("{# inline_template_start #}    <div class=\"container pi-60\">
        <div class=\"flex-chiffres\">
        <div class=\"container_chiffres\">
        <div class=\"icone_formation\"> <i> <img src=\"/idev/web/sites/default/files/media-icons/icone-tableau.png\" alt=\"Description of the icon\"  class=\"icone-chiffre\"></i></div>
        <div>
        <div class=\"chiffres\" id=\"value\">{{ field_chiffres_formations }}</div>
        <div class=\"titre_chiffres\">Formations effectuées</div>
        </div>
        </div>
        <div class=\"container_chiffres\" >
        <div class=\"icone_participant\"> <i> <img src=\"/idev/web/sites/default/files/media-icons/icone-people.png\" alt=\"Description of the icon\"  class=\"icone-chiffre\"></i></div>
        <div>
        <div class=\"chiffres\" id=\"value1\">{{ field_chif }}</div>
        <div class=\"titre_chiffres\">Participants </div>
         </div>
        </div>
        <div class=\"container_chiffres\" >
        <div class=\"icone_pays\"><i class=\"fa-regular fa-flag\"></i></div>
        <div>    
        <div class=\"chiffres\" id=\"value2\">{{ field_ch }}</div>
        <div class=\"titre_chiffres\">Pays</div>
        </div>
        </div>
        <div class=\"container_chiffres\">
            <div class=\"icone_mondats\"> <i> <img src=\"/idev/web/sites/default/files/media-icons/icone-fichier.png\" alt=\"Description of the icon\"  class=\"icone-chiffre\"></i></div>
               <div> 
            <div class=\"chiffres\" id=\"value3\">{{ field_chiffres_mondats_realises }}</div>
            <div class=\"titre_chiffres\">Mandats réalisés</div>
             </div>
            </div>
        </div>
        </div>", "__string_template__45012e97d66c0e844854053a27eccf54", "");
    }
    
    public function checkSecurity()
    {
        static $tags = array();
        static $filters = array("escape" => 6);
        static $functions = array();

        try {
            $this->sandbox->checkSecurity(
                [],
                ['escape'],
                []
            );
        } catch (SecurityError $e) {
            $e->setSourceContext($this->source);

            if ($e instanceof SecurityNotAllowedTagError && isset($tags[$e->getTagName()])) {
                $e->setTemplateLine($tags[$e->getTagName()]);
            } elseif ($e instanceof SecurityNotAllowedFilterError && isset($filters[$e->getFilterName()])) {
                $e->setTemplateLine($filters[$e->getFilterName()]);
            } elseif ($e instanceof SecurityNotAllowedFunctionError && isset($functions[$e->getFunctionName()])) {
                $e->setTemplateLine($functions[$e->getFunctionName()]);
            }

            throw $e;
        }

    }
}
